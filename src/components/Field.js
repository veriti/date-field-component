import React, { Component } from "react";
import format from "date-fns/format";

class Field extends Component {
  render() {
    const { cssPrefix, defaultDate, outputDateFormat, start, end, ...i } = this.props

    return (
      <div {...i} className={`${cssPrefix}__field`}>
        <span>
          {format(start, outputDateFormat)} - {format(end, outputDateFormat)}
        </span>
      </div>
    )
  }
}

Field.defaultProps = {
  cssPrefix: `df`,
  defaultDate: Date.now(),
  outputDateFormat: 'MMM DD, YYYY',
  start: Date.now(),
  end: Date.now(),
  onClick: () => {}
}

export default Field;
