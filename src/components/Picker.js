import React, { Component } from "react";
import format from "date-fns/format";
import isEqual from "date-fns/is_equal";
import addDays from "date-fns/add_days";
import startOfMonth from "date-fns/start_of_month";
import endOfMonth from "date-fns/end_of_month";
import startOfWeek from "date-fns/start_of_week";
import endOfWeek from "date-fns/end_of_week";
import eachDay from "date-fns/each_day";
import compareAsc from "date-fns/compare_asc";
import differenceInDays from "date-fns/difference_in_days";
import addMonths from "date-fns/add_months";
import subMonths from "date-fns/sub_months";
import ReactDOM from "react-dom";

class Picker extends Component {
  state = {
    viewDate: this.props.defaultDate,
    preselectStart: this.props.defaultDate,
    preselecting: false,
    preselectedDates: eachDay(
      this.props.defaultDate,
      addDays(this.props.defaultDate, 1)
    )
  };

  isDateBeforeMin = date => differenceInDays(date, this.props.minDate) < 0;
  isDateDisabled = date => this.props.disabledDates.some(d => isEqual(d, date));

  viewPrevMonth = () => {
    this.setState({ viewDate: subMonths(this.state.viewDate, 1) });
  };

  viewNextMonth = () => {
    this.setState({ viewDate: addMonths(this.state.viewDate, 1) });
  };

  selectDate = date => {
    const { preselecting, preselectedDates } = this.state;
    const { onChange } = this.props;

    if (this.isDateDisabled(date) || this.isDateBeforeMin(date)) return;

    const newDates = preselecting
      ? preselectedDates
      : eachDay(date, addDays(date, 1));

    this.setState({
      preselectStart: date,
      preselecting: !preselecting,
      preselectedDates: newDates
    });

    if (!preselecting) {
    }

    onChange(newDates);
  };

  preselectDate = date => {
    const { preselecting, preselectStart } = this.state;

    if (!preselecting) return;

    if (this.isDateDisabled(date) || this.isDateBeforeMin(date)) return;

    this.setState({
      preselectedDates: eachDay.apply(
        this,
        [preselectStart, date].sort(compareAsc)
      )
    });
  };

  abortSelection = () => {
    const { onAbort = () => {} } = this.props;
    onAbort();
  };

  createDateClass = date => {
    const { preselecting, preselectedDates } = this.state;
    const { cssPrefix } = this.props;

    const preselected = preselectedDates.some(d => isEqual(d, date));

    if (preselected) {
      return preselecting ? `${cssPrefix}__hilited` : `${cssPrefix}__selected`;
    }
  };

  render() {
    const { viewDate } = this.state;
    const { show, cssPrefix } = this.props;

    const calendarStart = startOfWeek(startOfMonth(viewDate));
    const calendarEnd = endOfWeek(endOfMonth(viewDate));
    const calendarDates = eachDay(calendarStart, calendarEnd);
    const visibilityClass = show ? `${cssPrefix}__picker--show` : ``;

    return (
      <React.Fragment>
        <PickerDim
          show={show}
          cssPrefix={cssPrefix}
          onClick={() => this.abortSelection()}
        />
        <div className={`${cssPrefix}__picker ${visibilityClass}`}>
          <div className={`${cssPrefix}__navigation`}>
            <span>{format(viewDate, "MMMM YYYY")}</span>

            <button
              type="button"
              className={`${cssPrefix}__navprev`}
              onClick={this.viewPrevMonth}
            />

            <button
              type="button"
              className={`${cssPrefix}__navnow`}
              onClick={() =>
                this.setState({ viewDate: this.props.defaultDate })
              }
            />

            <button
              type="button"
              className={`${cssPrefix}__navnext`}
              onClick={this.viewNextMonth}
            />
          </div>

          <div className={`${cssPrefix}__calendar`}>
            <div className={`${cssPrefix}__weekdays`}>
              {calendarDates
                .slice(0, 7)
                .map((date, i) => <div key={i}>{format(date, "ddd")}</div>)}
            </div>
            <div className={`${cssPrefix}__dates`}>
              {calendarDates.map((date, i) => {
                return (
                  <PickerDate
                    key={i}
                    cssPrefix={cssPrefix}
                    className={this.createDateClass(date)}
                    disabled={
                      this.isDateDisabled(date) || this.isDateBeforeMin(date)
                    }
                    onClick={() => this.selectDate(date)}
                    onMouseEnter={() => this.preselectDate(date)}
                    onFocus={() => this.preselectDate(date)}
                    date={date}
                  />
                );
              })}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

Picker.defaultProps = {
  cssPrefix: "df",
  defaultDate: Date.now(),
  minDate: Date.now(),
  outputDateFormat: "MMM DD, YYYY",
  startDateFieldName: "start_date",
  endDateFieldName: "end_date",
  disabledDates: []
};

const PickerDate = ({
  date,
  cssPrefix,
  disabled,
  hilited,
  hiliteType,
  ...rest
}) => {
  if (disabled) {
    return (
      <div {...rest}>
        <span>{format(date, "D")}</span>
      </div>
    );
  }

  return (
    <button type="button" {...rest}>
      <span>{format(date, "D")}</span>
    </button>
  );
};

class PickerDim extends Component {
  render() {
    const { show, cssPrefix, onClick } = this.props;

    return ReactDOM.createPortal(
      <div className={show ? `${cssPrefix}__dim` : ``} onClick={onClick} />,
      document.querySelector("body")
    );
  }
}

export default Picker;
