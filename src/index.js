import React from 'react'
import ReactDOM from 'react-dom'
import DateField from './DateField'
import registerServiceWorker from './registerServiceWorker'

const rootEl = document.querySelector('[data-datefield-config]')
const config = window[rootEl.getAttribute('data-datefield-config')]

ReactDOM.render(<DateField {...config}/>, rootEl)

if (module.hot) {
  module.hot.accept('./DateField', () => {
    const NewDateField = require('./DateField').default
    ReactDOM.render(<NewDateField {...config}/>, rootEl)
  })
}

registerServiceWorker();

