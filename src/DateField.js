import React, { Component } from "react";
import Field from "./components/Field";
import Picker from "./components/Picker";
import format from "date-fns/format";
import addDays from "date-fns/add_days";
import isEqual from "date-fns/is_equal";

import "./DateField.css";

class DateField extends Component {
  state = {
    showPicker: false,
    start: addDays(window.DATEFIELD_CONFIG.defaultDate, 0),
    end: addDays(window.DATEFIELD_CONFIG.defaultDate, 1),
  };

  onPickerChange = dates => {
    const { outputDateFormat, disabledDates, onChange } = this.props

    this.setState({
      start: format(dates[0], outputDateFormat),
      end: format(dates[dates.length - 1], outputDateFormat)
    })

    onChange({
      selectedDates: dates,
      disabledDates: dates.filter(d => disabledDates.some(dd => isEqual(d, dd)))
    })
  };

  onFieldClick = () => {
    this.setState({ showPicker: !this.state.showPicker });
  };

  render() {
    const { start, end, showPicker } = this.state;
    const {
      cssPrefix,
      defaultDate,
      minDate,
      disabledDates,
      outputDateFormat,
      startDateFieldName,
      endDateFieldName,
    } = this.props

    return (
      <div className={cssPrefix}>
        <input type="hidden" name={startDateFieldName} value={start} />

        <input type="hidden" name={endDateFieldName} value={end} />

        <Field
          cssPrefix={cssPrefix}
          defaultDate={defaultDate}
          outputDateFormat={outputDateFormat}
          start={start}
          end={end}
          onClick={this.onFieldClick}
        />

        <Picker
          cssPrefix={cssPrefix}
          defaultDate={defaultDate}
          minDate={minDate}
          disabledDates={disabledDates}
          onChange={this.onPickerChange}
          onAbort={() => this.setState({ showPicker: !showPicker })}
          show={showPicker}
        />

      </div>
    );
  }
}

export default DateField;
